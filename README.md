PayconiqTest
============

In general for the project i use Cocoapods to manage the dependecies, in this case i use one 2 dependencies:
- RxSwift
- RxCocoa

**First assignment**

I use the Reactive programming here just to show that is useful and easy to implement, in this case to handle the tap action of the buttons.
I implemented 2 things, first one is a presenter for alerts where you can define the style, title, message and add the needed buttons with it's actions.
I use the method chain for a easy readability and fast usability, frameworks like RxSwift, Alamofire use this kind of methods.
I created a custom alert using the same method chain, a lot of apps like to develop their own alerts UI, so, i developed a fast and simple custom alerts that include a title, message and possibility to add buttons, like a regular alert but custom design, in the presenter you can choose if you want a regular alert, an actionSheet alert or a custom alert, in the 'HomeViewController' class you can see the implementation and mode of use of the alert presenter.

**Second assignment**

Here i liked to use a simple way to obfuscate a string, so, i choose those:
- Xor, applying a xor operator for the utf8 value of each character of the string, so, you will get a no readable string, for this the helper will create a random key for use it in the operation
- Skip, i don't really know if this is a "official" algorithm for encryption or so, but i called like that; here i get the ascii code for each character of the string and add a random value making a complete different character, making at the end a no readable string.

I created a class for the string, the class never save the original value of the string, just know they key and the method of obfuscation used to revert it, this class also have the expiration date, of you are trying to use the string after the expiration date it will return an error.
In order of a right use of the value i added a try catch for the value, so, you can know if the value expired or you are creating the value not with the helper.
Helper is a singleton that live as much as the app, so, when the app is up you have to setup what kind of obfuscation want to use, helper will use it while is live, also you can purge the helper, so, all of the string will be purged and removed as well.
Random keys are created when helper is created for the first time, if you purge the helper key will be regenerated again.
