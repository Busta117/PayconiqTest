//
//  HomeViewController.swift
//  PayconiqTest
//
//  Created by Santiago Bustamante on 3/9/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {

    @IBOutlet private weak var tryAlertButton: UIButton!
    @IBOutlet private weak var tryActionSheetButton: UIButton!
    @IBOutlet private weak var tryCustomAlertButton: UIButton!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let myValue = ObfuscationHelper.shared.obfuscate(string: "hello world", expireIn: 60)
        print(try! myValue.obfuscated())
        print(try! myValue.unobfuscated())
        
                
//        let myValue2 = ObfuscatedString.init("Hello world 2", expireIn: 0)
//        do {
//            let str = try myValue2.obfuscated()
//            print(str)
//        } catch {
//            print("error \(error.localizedDescription)")
//        }
        
        
        dataBinding()
        
    }

    private func dataBinding() {
        tryAlertButton.rx.tap.subscribe({ _ in
            self.showAlert(style: .alert)
        }).disposed(by: disposeBag)
        
        tryActionSheetButton.rx.tap.subscribe({ _ in
            self.showAlert(style: .actionSheet)
        }).disposed(by: disposeBag)
        
        tryCustomAlertButton.rx.tap.subscribe({ _ in
            self.showAlert(style: .custom)
        }).disposed(by: disposeBag)
    }

    private func showAlert(style: AlertControllerStyle) {
        AlertPresenter(title: "SO AMAZING TITLE", message: "This is an awesome informative message... \nlove it ❤️", preferredStyle: style, viewController: self)
            .addButton("Ok") {
                print("user pressed Ok button, lets do something fun")
            }.addButton("Cancel", style: .cancel)
            .show()
    }
    
}
