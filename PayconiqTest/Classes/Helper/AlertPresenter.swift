//
//  AlertPresenter.swift
//  PayconiqTest
//
//  Created by Santiago Bustamante on 3/9/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import UIKit

/// alert style, same as UIAlertControllerStyle but adding the custom option
enum AlertControllerStyle: Int {
    case actionSheet, alert, custom
}

 /// Helper to present native and custom alerts, custom alert is presented as PayconiqAlert
 /// Using Chaining Methods
class AlertPresenter {
    
    private typealias closureAction = () -> ()
    
    private var title: String?
    private var message: String?
    private var viewController: UIViewController?
    private var buttons: [UIAlertAction] = [UIAlertAction]()
    private var actions: [closureAction] = [closureAction]()
    private var preferredStyle: AlertControllerStyle = .alert
    
    public init(title:String? = "", message:String?,
                preferredStyle: AlertControllerStyle = .alert,
                viewController: UIViewController?) {
        self.title = title
        self.message = message
        self.viewController = viewController
        self.preferredStyle = preferredStyle
    }
    
    /// Add a button to the current alert
    ///
    /// - parameter title:      Alert title.
    /// - parameter style:      style of the button(UIAlertActionStyle), default .default
    /// - parameter action:     action for the button, default nil
    ///
    /// - returns: The new `AlertPresenter` instance.
    @discardableResult
    func addButton(_ title: String, style: UIAlertActionStyle = .default, action: (()->())? = nil) -> AlertPresenter {
        
        let butAction = UIAlertAction(title: title, style: style) { (actionIn) -> Void in
            action?()
        }
        buttons.append(butAction)
        actions.append(action ?? {}) //if there is no action add a fake action to prevent nil access
        return self
    }
    
    ///present the alert in the viewController especified in the init
    func show() {
        guard let viewController = viewController else {
            return
        }
        
        if preferredStyle == .custom {
            let alert = PayconiqAlert(title: title, message: message ?? "")
            for (i, but) in buttons.enumerated() {
                alert.addButton(but.title ?? "", style: but.style, action: actions[i])
            }
            alert.show(from: viewController)
            return
        }
        
        let style = UIAlertControllerStyle(rawValue: preferredStyle.rawValue) ?? .alert
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for but in buttons {
            alert.addAction(but)
        }
        viewController.present(alert, animated: true, completion: nil)
    }
    
}


