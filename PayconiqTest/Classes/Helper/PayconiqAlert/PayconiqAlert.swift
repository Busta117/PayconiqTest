//
//  PayconiqAlert.swift
//  PayconiqTest
//
//  Created by Santiago Bustamante on 3/9/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import UIKit

/// Custom alert implementation
/// Using Chaining Methods
class PayconiqAlert: UIViewController {

    private typealias closureAction = () -> ()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet private weak var containerStackView: UIStackView!
    
    private var buttons: [UIButton] = [UIButton]()
    private var actions: [Int: closureAction] = [Int: closureAction]()
    
    private var titleStr: String? //property `title` is reserved by UIViewController
    private var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = titleStr
        messageLabel.text = message
        
        for butAction in buttons {
            containerStackView.addArrangedSubview(butAction)
            butAction.heightAnchor.constraint(equalToConstant: 40).isActive = true 
        }
        
    }
    
    /// Create the alert using the title and message from the parameters
    /// using the reserved keyword `init` to allow using it as real init
    /// example:
    ///
    /// PayconiqAlert(title: "this title", message: "alert message here")
    ///
    /// - returns: The new `PayconiqAlert` instance.
    @discardableResult
    class func `init`(title: String? = nil, message: String = "") -> PayconiqAlert {
        let alert = UIStoryboard(name: "PayconiqAlert", bundle: nil).instantiateViewController(withIdentifier: "PayconiqAlert") as! PayconiqAlert
        alert.titleStr = title
        alert.message = message
        return alert
    }
    
    /// Add a button to the current alert
    /// Currently accept as much buttons as you want but be careful, if you add a lot of buttons alert will be higher than the screen size
    ///
    /// - parameter title:      Alert title.
    /// - parameter style:      style of the button(UIAlertActionStyle), default .default
    /// - parameter action:     action for the button, default nil
    ///
    /// - returns: The new `PayconiqAlert` instance.
    @discardableResult
    func addButton(_ title: String, style: UIAlertActionStyle = .default, action: (()->())? = nil) -> PayconiqAlert {
        let butAction = UIButton(frame: .zero)
        butAction.setTitle(title, for: .normal)
        
        switch style {
        case .cancel, .destructive:
            butAction.backgroundColor = UIColor(red: 207.0/255.0, green: 92.0/255.0, blue: 78.0/255.0, alpha: 1)
        default:
            butAction.backgroundColor = UIColor(red: 142.0/255.0, green: 192.0/255.0, blue: 117.0/255.0, alpha: 1)
        }
        
        butAction.cornerRadius = 3
        buttons.append(butAction)
        butAction.tag = buttons.count - 1
        butAction.addTarget(self, action: #selector(buttonAction(_:)), for: UIControlEvents.touchUpInside)
        actions[butAction.tag] = action
        
        return self
    }
    
    /// Private method
    /// the action of each button on the alert, identifying each button for the tag property
    @objc private func buttonAction(_ sender: UIButton) {
        actions[sender.tag]?()
        dismiss(animated: false, completion: nil)
    }
    
    /// Present the current alert over the viewController on the parameter
    func show(from: UIViewController) {
        from.present(self, animated: false, completion: nil)
    }
    
    ///deallocation of the controller
    deinit {
        print("*********Deallocating \(Mirror(reflecting: self).subjectType) ****************")
    }
}

/// extension of the UIView with options changables directly on the storyboard
@IBDesignable
extension UIView {
    
    /// allow to set a corner radius directly from the Storyboard file
    @IBInspectable
    var cornerRadius : CGFloat {
        set {
            layer.masksToBounds = true
            layer.cornerRadius =  newValue
        }
        get { return layer.cornerRadius }
    }
}
