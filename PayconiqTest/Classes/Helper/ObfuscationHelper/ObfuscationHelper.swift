//
//  ObfuscationHelper.swift
//  PayconiqTest
//
//  Created by Santiago Bustamante on 3/10/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import UIKit

enum ObfuscationType {
    case xor, skip
}

enum ObfuscationError: Error, LocalizedError {
    case valueExpired
    case notFromHelper
    
    public var errorDescription: String? {
        switch self {
        case .valueExpired:
            return NSLocalizedString("value is not longer valid, reached expiration time", comment: "")
        case .notFromHelper:
            return NSLocalizedString("Object need to be created thru ObfuscationHelper", comment: "")
        }
    }
}

/// object to load obfuscate and delete a sensitve string
/// this object never save the original string
class ObfuscatedString {
    
    fileprivate var id: Int
    private var privateValue: String
    fileprivate private(set) var expirationDate: Date
    fileprivate var inHelper = false
    
    
    init(_ value: String, expireIn: TimeInterval) {
        expirationDate = Date(timeIntervalSinceNow: expireIn)
        privateValue = value
        id = Int(arc4random())
        obfuscate()
    }
   
    /// Method to apply the obfuscatation to the string
    /// Obfuscation algorithm depends of the configuration of the ObfuscationHelper
    private func obfuscate() {
        if ObfuscationHelper.shared.obfuscationType == .skip {
            privateValue = privateValue.unicodeScalars.filter{$0.isASCII}.map{$0.value}.map{$0 + 5}.map{"\(UnicodeScalar($0)!)"}.joined()
        } else {
            let key = ObfuscationHelper.shared.key
            privateValue = String(bytes: privateValue.utf8.map{$0 ^ key}, encoding: String.Encoding.utf8) ?? ""
        }
    }
    
    /// alias to load the obfuscated value
    func value() throws -> String {
        return try obfuscated()
    }
    
    /// returns the obfuscated string if value is not expired
    /// if the object was created outside of the helper will returns an error
    func obfuscated() throws -> String {
        
        guard inHelper else {
            ObfuscationHelper.shared.remove(self)
            throw ObfuscationError.notFromHelper
        }
        
        if Date() > expirationDate {
            ObfuscationHelper.shared.remove(self)
            throw ObfuscationError.valueExpired
        }
        return privateValue
    }
    
    ///try to use the unobfuscated (plain) string, will decode the string if is not expired
    /// if the object was created outside of the helper will returns an error
    func unobfuscated() throws -> String {
        
        guard inHelper else {
            ObfuscationHelper.shared.remove(self)
            throw ObfuscationError.notFromHelper
        }
        
        if Date() > expirationDate {
            ObfuscationHelper.shared.remove(self)
            throw ObfuscationError.valueExpired
        }
        
        if ObfuscationHelper.shared.obfuscationType == .skip {
            return privateValue.unicodeScalars.filter{$0.isASCII}.map{$0.value}.map{$0 - 5}.map{"\(UnicodeScalar($0)!)"}.joined()
        } else {
            let key = ObfuscationHelper.shared.key
            return String(bytes: privateValue.utf8.map{$0 ^ key}, encoding: String.Encoding.utf8) ?? ""
        }
    }
    
    ///remove all data of the object
    @discardableResult
    fileprivate func purged() -> ObfuscatedString {
        privateValue = ""
        expirationDate = Date(timeIntervalSince1970: 0)
        return self
    }
    
}


/// Helper to create and handler the ObfuscatedString objects
/// always create a obfuscated string thru the helper
class ObfuscationHelper {
    
    static let shared = ObfuscationHelper()
    var obfuscationType = ObfuscationType.xor
    fileprivate var key: UInt8
    fileprivate var skipValue: Int
    fileprivate var obfuscatedValues = [ObfuscatedString]()
    
    
    init() {
        key = ObfuscationHelper.randomKey()
        skipValue = Int(arc4random_uniform(15))
    }
    
    /// create a random key for the obfuscation algorithm
    private class func randomKey() -> UInt8 {
        var randomBytes: [UInt8] = [0]
        _ = SecRandomCopyBytes(kSecRandomDefault, 1, &randomBytes)
        return randomBytes.first!
    }
    
    /// create an obfuscate string
    func obfuscate(string value: String, expireIn: TimeInterval) -> ObfuscatedString {
        let obfuscated = ObfuscatedString(value, expireIn: expireIn)
        obfuscated.inHelper = true
        obfuscatedValues.append(obfuscated)
        return obfuscated
    }
    
    /// remove only one object from the helper
    fileprivate func remove(_ value: ObfuscatedString) {
        var index = -1
        for (i, string) in obfuscatedValues.enumerated() {
            if value.id == string.id {
                index = i
                break
            }
        }
        if index >= 0 {
            value.purged()
            obfuscatedValues.remove(at: index)
        }
    }
    
    //clean all the helper, restar all the private keys
    func purge() {
        obfuscatedValues = obfuscatedValues.map({$0.purged()})
        obfuscatedValues.removeAll()
        key = ObfuscationHelper.randomKey()
        skipValue = Int(arc4random_uniform(15))
    }
    
    
}


