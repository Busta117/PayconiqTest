//
//  PayconiqTestTests.swift
//  PayconiqTestTests
//
//  Created by Santiago Bustamante on 3/9/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import XCTest
@testable import PayconiqTest

class PayconiqTestTests: XCTestCase {
    
     //var myValue: ObfuscatedString?
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    
    func testXorObfuscation() {
        ObfuscationHelper.shared.obfuscationType = .xor
        
        let initialString = "hello world"
        let myValue = ObfuscationHelper.shared.obfuscate(string: initialString, expireIn: 15)
        
        XCTAssertEqual(initialString, myValue.unobfuscated())
        
        
        let waitExpectation = expectation(description: "Waiting")
        let when = DispatchTime.now() + 16
        DispatchQueue.main.asyncAfter(deadline: when) {
            XCTAssertNil(myValue.unobfuscated())
            waitExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 17)
        
    }
    
    func testSkipObfuscation() {
        ObfuscationHelper.shared.obfuscationType = .skip
        
        let initialString = "hello world"
        let myValue = ObfuscationHelper.shared.obfuscate(string: initialString, expireIn: 15)
        
        XCTAssertEqual(initialString, myValue.unobfuscated())
        
        let waitExpectation = expectation(description: "Waiting")
        let when = DispatchTime.now() + 16
        DispatchQueue.main.asyncAfter(deadline: when) {
            XCTAssertNil(myValue.unobfuscated())
            waitExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 17)
    }
    
}
