//
//  PayconiqTestUITests.swift
//  PayconiqTestUITests
//
//  Created by Santiago Bustamante on 3/9/18.
//  Copyright © 2018 Santiago Bustamante. All rights reserved.
//

import XCTest

class PayconiqTestUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAlerts() {
        alertTest()
        actionSheetTest()
        customAlertTest()
    }
    
    
    func alertTest() {
        let app = XCUIApplication()
        app.buttons["Try Alert!!"].tap()
        XCTAssert(app.buttons["Ok"].exists, "Alert didn't show")
        app.buttons["Ok"].tap()
        XCTAssert(app.buttons["Try Alert!!"].exists, "Alert didn't desappear")
    }
    
    func actionSheetTest() {
        let app = XCUIApplication()
        app.buttons["Try ActionSheet!!"].tap()
        XCTAssert(app.buttons["Ok"].exists, "Alert didn't show")
        app.buttons["Ok"].tap()
        XCTAssert(app.buttons["Try ActionSheet!!"].exists, "Alert didn't desappear")
    }
    
    func customAlertTest() {
        let app = XCUIApplication()
        app.buttons["Try Custom Alert!!"].tap()
        XCTAssert(app.buttons["Ok"].exists, "Alert didn't show")
        app.buttons["Ok"].tap()
        XCTAssert(app.buttons["Try Custom Alert!!"].exists, "Alert didn't desappear")
    }
}
